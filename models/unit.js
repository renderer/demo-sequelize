/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  var unit = sequelize.define('unit', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'unit',
    timestamps: false,
    freezeTableName: true
  });

  return unit;
};
