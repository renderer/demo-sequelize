var Sequelize = require('sequelize');
var sequelize = new Sequelize('test', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

var User = sequelize.import('./models/user');
var Unit = sequelize.import('./models/unit');

var Users = Unit.hasMany(User, { as: 'Users' });

Unit.findAll({ include: [Users] }).then(function(units) {
  console.log(JSON.stringify(units));
});

User.findAll({ include: [Unit] }).then(function(users) {
  console.log(JSON.stringify(users));
});